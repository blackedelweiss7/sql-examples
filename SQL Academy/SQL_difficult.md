## Сложность - сложные задачи

#### 1. 
Найдите максимальный возраст (колич. лет) среди обучающихся 10 классов ? 
Ссылка на задачу: https://sql-academy.org/ru/trainer/tasks/44
```sql
select 
       max(TIMESTAMPDIFF(YEAR, birthday,CURRENT_DATE)) as max_year 
from Student_in_class
                     inner join Class
                            on Student_in_class.class = Class.id
                     inner join Student
                            on Student_in_class.student = Student.id
where Class.name like '10%'
```
#### 2.
Какой(ие) кабинет(ы) пользуются самым большим спросом?
Ссылка на задачу: https://sql-academy.org/ru/trainer/tasks/45

```sql
select classroom
from Schedule
group by classroom
having count(*) = (select max(cnt)
                   from (
                            select classroom,
                                   count(*) as cnt
                            from Schedule
                            group by classroom)t)
```
#### 3.
Удалить компании, совершившие наименьшее количество рейсов.
Ссылка на задачу: https://sql-academy.org/ru/trainer/tasks/55
```sql
delete from Company
where Company.id in 
                    (select Company
                    from (
                            select company,
                                   count(*) as cnt
                            from Trip
                            group by company)t
                    where cnt = (select min(cnt)
                                 from (
                                         select company,
                                           count(*) as cnt
                                        from Trip
                                        group by company)t2))
```
#### 4.
Выведите идентификаторы преподавателей, которые хотя бы один раз за всё время преподавали в каждом из одиннадцатых классов.
Ссылка на задачу: https://sql-academy.org/ru/trainer/tasks/60
```sql
select teacher
from Schedule
             inner join Class    
                   on Schedule.class = Class.id
where Class.name like '11%'
group by teacher
having count(DISTINCT Class.name) = (select max(cnt_distinct)
                                     from (
                                        select teacher,
                                               count(DISTINCT Class.name) as cnt_distinct
                                        from Schedule
                                                     inner join Class    
                                                           on Schedule.class = Class.id
                                        where Class.name like '11%'
                                        group by teacher)t)
```

#### 5.
Выведите имена всех пар пассажиров, летевших вместе на одном рейсе два или более раз, и количество таких совместных рейсов. В passengerName1 разместите имя пассажира с наименьшим идентификатором.
Ссылка на задачу: https://sql-academy.org/ru/trainer/tasks/64
```sql
with pairs as (
                select 
                       t.passenger pas, 
                       t1.passenger pas1,
                       count(distinct t.trip) as cnt
                from Pass_in_trip t
                                cross join Pass_in_trip t1
                                  on t.trip = t1.trip 
                                  and t.passenger != t1.passenger
                                  and t.passenger < t1.passenger
                GROUP BY pas, pas1),
        
        first_name as (
                    select Passenger.name as passengerName1,
                           pas1, 
                           cnt     
                    from pairs
                              inner join Passenger
                                  on pairs.pas = Passenger.id)
                                  
                                  
 select passengerName1,
        name as passengerName2,
        cnt
 from first_name 
                inner join Passenger
                      on first_name.pas1 = Passenger.id
                                  
 where count >=2
```
