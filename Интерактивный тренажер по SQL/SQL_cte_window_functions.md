#### 1. 
Вычислить рейтинг каждого студента относительно студента, прошедшего наибольшее количество шагов в модуле (вычисляется как отношение количества пройденных студентом шагов к максимальному количеству пройденных шагов, умноженное на 100). Вывести номер модуля, имя студента, количество пройденных им шагов и относительный рейтинг. Относительный рейтинг округлить до одного знака после запятой. Столбцы назвать Модуль, Студент, Пройдено_шагов и Относительный_рейтинг  соответственно. Информацию отсортировать сначала по возрастанию номера модуля, потом по убыванию относительного рейтинга и, наконец, по имени студента в алфавитном порядке. 

```sql
with number_steps as (
                        SELECT student_name as Студент, 
                               module_id Модуль,
                               count(DISTINCT step_id) as Пройдено_шагов
                         FROM step_student INNER JOIN step USING (step_id)
                                           INNER JOIN lesson USING (lesson_id)
                                           inner join student on step_student.student_id =  student.student_id  
                        WHERE result = "correct"
                        GROUP BY student_name, module_id)

select Модуль,
       Студент, 
       Пройдено_шагов,
       round((Пройдено_шагов / MAX(Пройдено_шагов) OVER (PARTITION BY Модуль)) * 100, 1) as Относительный_рейтинг
from number_steps
order by Модуль asc, Относительный_рейтинг desc, Студент asc
```

#### 2. 
Вывести жанр(ы), в котором было заказано меньше всего экземпляров книг, указать это количество. Учитывать только жанры, в которых была заказана хотя бы одна книга.

```sql
with genre_orders as (
                        select name_genre as name_genre,  
                               sum(buy_book.amount) as Количество
                        from buy_book
                                    inner join book
                                          on buy_book.book_id = book.book_id
                                    inner join genre 
                                          on book.genre_id = genre.genre_id
                        group by name_genre
                        having sum(buy_book.amount) >= 1)
                        
select name_genre,
       Количество
from genre_orders
where Количество = (select min(Количество) from genre_orders)
```