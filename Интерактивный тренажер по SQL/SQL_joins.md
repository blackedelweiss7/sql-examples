
#### 1. 
Вывести название, жанр и цену тех книг, количество которых больше 8, в отсортированном по убыванию цены виде.
```sql
SELECT title, 
       name_genre, 
       price
FROM 
    genre 
         INNER JOIN book
               ON genre.genre_id = book.genre_id
WHERE book.amount > 8
ORDER BY price DESC;
```
#### 2. 
Вывести информацию о книгах (жанр, книга, автор), относящихся к жанру, включающему слово «роман» в отсортированном по названиям книг виде.
```sql
SELECT name_genre, 
       title, 
       name_author
FROM 
    genre
         INNER JOIN book 
               ON book.genre_id = genre.genre_id
         INNER JOIN author 
               ON author.author_id = book.author_id
WHERE name_genre = 'Роман'
ORDER BY title;
```
#### 3. 
Посчитать количество экземпляров  книг каждого автора из таблицы author.  Вывести тех авторов,  количество книг которых меньше 10, в отсортированном по возрастанию количества виде. Последний столбец назвать Количество.
```sql
SELECT name_author, 
       SUM(amount) AS Количество
FROM author 
            LEFT JOIN book
                 ON author.author_id = book.author_id
GROUP BY name_author
HAVING SUM(amount) < 10 OR SUM(amount) IS Null 
ORDER BY Количество;   
```
#### 4. 
Посчитать количество экземпляров  книг каждого автора из таблицы author.  Вывести тех авторов,  количество книг которых меньше 10, в отсортированном по возрастанию количества виде. Последний столбец назвать Количество.
```sql
SELECT name_author
FROM 
    author
          INNER JOIN book 
                ON author.author_id = book.author_id
          INNER JOIN genre
                ON genre.genre_id = book.genre_id
GROUP BY name_author
HAVING COUNT(DISTINCT name_genre) = 1; 
```

#### 5. 
Если в таблицах supply  и book есть одинаковые книги, которые имеют равную цену,  вывести их название и автора, а также посчитать общее количество экземпляров книг в таблицах supply и book,  столбцы назвать Название, Автор  и Количество.
```sql
SELECT book.title AS Название,
       name_author AS Автор, 
       (book.amount + supply.amount) AS Количество
FROM 
    author 
           INNER JOIN book 
                 USING (author_id)   
           INNER JOIN 
                 supply ON book.title = supply.title 
                           and book.price = supply.price;
```
#### 6. 
Посчитать, сколько раз была заказана каждая книга, для книги вывести ее автора (нужно посчитать, в каком количестве заказов фигурирует каждая книга).  Вывести фамилию и инициалы автора, название книги, последний столбец назвать Количество. Результат отсортировать сначала  по фамилиям авторов, а потом по названиям книг.
```sql
SELECT author.name_author,
       book.title,
       IF (COUNT(buy_book.amount) is NULL, 0, COUNT(buy_book.amount)) AS Количество
FROM 
    author 
          INNER JOIN book 
                     ON author.author_id = book.author_id
          LEFT JOIN buy_book 
                     ON book.book_id = buy_book.book_id
GROUP BY author.name_author, book.title
ORDER BY author.name_author, book.title
```
#### 7. 
Вывести жанр (или жанры), в котором было заказано больше всего экземпляров книг, указать это количество . Последний столбец назвать Количество.
```sql
SELECT genre.name_genre,
       SUM(buy_book.amount) AS Количество
FROM genre
          INNER JOIN book 
                USING (genre_id)
          INNER JOIN buy_book
                USING (book_id)
GROUP BY genre.name_genre
HAVING SUM(buy_book.amount) = 
                              (SELECT MAX(Количество)
                               FROM 
                                   (SELECT SUM(buy_book.amount) AS Количество
                                    FROM genre
                                              INNER JOIN book 
                                                    USING (genre_id)
                                              INNER JOIN buy_book 
                                                    USING (book_id)
                                              GROUP BY genre.name_genre
                                    ) genre_maximum);
```
#### 8. 
Сравнить ежемесячную выручку от продажи книг за текущий и предыдущий годы. Для этого вывести год, месяц, сумму выручки в отсортированном сначала по возрастанию месяцев, затем по возрастанию лет виде. Название столбцов: Год, Месяц, Сумма.
```sql
SELECT YEAR(date_payment) AS Год, 
       MONTHNAME(date_payment) AS Месяц,
       SUM(buy_archive.amount * price) AS Сумма
FROM 
    buy_archive
GROUP BY YEAR(date_payment), MONTHNAME(date_payment)
UNION ALL
SELECT YEAR(date_step_end), 
       MONTHNAME(date_step_end),
       SUM(buy_book.amount * price) AS Сумма
FROM 
    book 
        INNER JOIN buy_book 
              USING(book_id)
        INNER JOIN buy 
              USING(buy_id) 
        INNER JOIN buy_step 
              USING(buy_id)
        INNER JOIN step 
              USING(step_id)
WHERE date_step_end IS NOT Null and name_step = "Оплата"
GROUP BY YEAR(date_step_end), MONTHNAME(date_step_end)
ORDER BY 2, 1
```

#### 9. 
Сравнить ежемесячную выручку от продажи книг за текущий и предыдущий годы. Для этого вывести год, месяц, сумму выручки в отсортированном сначала по возрастанию месяцев, затем по возрастанию лет виде. Название столбцов: Год, Месяц, Сумма.
```sql
SELECT YEAR(date_payment) AS Год, 
       MONTHNAME(date_payment) AS Месяц,
       SUM(buy_archive.amount * price) AS Сумма
FROM 
    buy_archive
GROUP BY YEAR(date_payment), MONTHNAME(date_payment)
UNION ALL
SELECT YEAR(date_step_end), 
       MONTHNAME(date_step_end),
       SUM(buy_book.amount * price) AS Сумма
FROM 
    book 
        INNER JOIN buy_book 
              USING(book_id)
        INNER JOIN buy 
              USING(buy_id) 
        INNER JOIN buy_step 
              USING(buy_id)
        INNER JOIN step 
              USING(step_id)
WHERE date_step_end IS NOT Null and name_step = "Оплата"
GROUP BY YEAR(date_step_end), MONTHNAME(date_step_end)
ORDER BY 2, 1
```
#### 10. 
Вывести, сколько попыток сделали студенты по каждой дисциплине, а также средний результат попыток, который округлить до 2 знаков после запятой. Под результатом попытки понимается процент правильных ответов на вопросы теста, который занесен в столбец result.  В результат включить название дисциплины, а также вычисляемые столбцы Количество и Среднее. Информацию вывести по убыванию средних результатов.
```sql
SELECT subject.name_subject,
       COUNT(date_attempt) AS Количество,
       ROUND(AVG (result), 2) AS Среднее
FROM subject
            LEFT JOIN attempt 
                 USING (subject_id)
GROUP BY subject.name_subject
ORDER BY ROUND(AVG (result), 2) DESC;
```
#### 11. 
Вывести студентов (различных студентов), имеющих максимальные результаты попыток . Информацию отсортировать в алфавитном порядке по фамилии студента.
```sql
SELECT name_student,
       result
FROM student
             INNER JOIN attempt 
                   USING (student_id)
WHERE result = (
               SELECT MAX(result)
               FROM attempt)
ORDER BY name_student;
```

#### 12. 
Вывести студентов (различных студентов), имеющих максимальные результаты попыток . Информацию отсортировать в алфавитном порядке по фамилии студента.
```sql
SELECT name_student,
       result
FROM student
             INNER JOIN attempt 
                   USING (student_id)
WHERE result = (
               SELECT MAX(result)
               FROM attempt)
ORDER BY name_student;
```